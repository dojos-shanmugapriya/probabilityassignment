package com.thoughtworks.vapasi.probability;


import java.util.Objects;

public class MathProbability {
    private static final Double MIN_RANGE = new Double(0);
    private static final Double MAX_RANGE = new Double(1);

    private Double probabilityOfEvent;

    public MathProbability(Double probabilityOfEvent) {
            this.probabilityOfEvent = probabilityOfEvent;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass())
             return false;
        else {
            MathProbability mathProbability = (MathProbability) obj;
            try {
                if (validate() && mathProbability.validate())
                    return (this.probabilityOfEvent.compareTo(mathProbability.probabilityOfEvent)) == 0;
            } catch (InvalidProbabilityException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean validate() throws InvalidProbabilityException {
        if (!(probabilityOfEvent >= MIN_RANGE && probabilityOfEvent <= MAX_RANGE) )
                throw new InvalidProbabilityException("Invalid Probability");
        return true;
    }
    @Override
    public int hashCode() {
        return Objects.hash(this.probabilityOfEvent.hashCode());
    }

    public Double inverse() {
        return (1 - this.probabilityOfEvent);
    }

    public Double combinedProbabilityOfIndependentEvents(MathProbability mathProbability) {
        return this.probabilityOfEvent * mathProbability.probabilityOfEvent;
    }

    public Double oneOrAnotherEventCouldHappen(MathProbability mathProbability) {
        return this.probabilityOfEvent +  mathProbability.probabilityOfEvent - this.combinedProbabilityOfIndependentEvents(mathProbability);
    }

    public Double xorOfTwoProbabilities(MathProbability mathProbability) {
        return (this.probabilityOfEvent + mathProbability.probabilityOfEvent - 2 * (this.combinedProbabilityOfIndependentEvents(mathProbability)));
    }
    public Double bayesianProbability(MathProbability mathProbability, double probabilityOfEventGivenAnotherEvent){
        return ((probabilityOfEventGivenAnotherEvent * probabilityOfEvent ) / mathProbability.probabilityOfEvent);
        }



}
