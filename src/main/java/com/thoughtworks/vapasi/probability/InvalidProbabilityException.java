package com.thoughtworks.vapasi.probability;

public class InvalidProbabilityException extends Exception{
    public InvalidProbabilityException(String message) {
        super(message);

    }
}
