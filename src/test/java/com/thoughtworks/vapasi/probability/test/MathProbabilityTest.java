package com.thoughtworks.vapasi.probability.test;

import com.thoughtworks.vapasi.probability.InvalidProbabilityException;
import com.thoughtworks.vapasi.probability.MathProbability;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MathProbabilityTest {

    @Test
    public void shouldCheckEqualityOfProbability() {
        assertEquals(true,new
                MathProbability(0.5).
                equals(new MathProbability(0.5)));
    }

    @Test
    public void shouldCheckInverseProbability() {
        assertEquals(0.5, new MathProbability(0.5).inverse());
    }

    @Test
    public void shouldCheckCombinedProbabilityOfIndependentEvents() {
        assertEquals(0.2,new MathProbability(0.5).combinedProbabilityOfIndependentEvents(new MathProbability(0.4)));
    }

    @Test
    public void shouldCheckIfOneOrAnotherEventCouldHappen() {
        assertEquals(0.7,new MathProbability(0.5).oneOrAnotherEventCouldHappen(new MathProbability(0.4)));
    }

    @Test
    public void shouldCheckXorOfTwoProbabilities() {
        assertEquals(0.58,
                new MathProbability(0.3).
                        xorOfTwoProbabilities(
                                new MathProbability(0.7)),0.01);
    }

    @Test
    public void shouldCheckBayesianProbability(){
        assertEquals(0.16, new MathProbability(0.1).bayesianProbability(new MathProbability(0.05),0.08),0.01);
    }

    @Test
    public void shouldCheckIfAGivenProbabilityIsValid() {
        try {
            assertTrue(new MathProbability(0.5).validate());
        } catch (InvalidProbabilityException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldThrowExceptionIfAGivenProbabilityIsInvalid() {
        assertThrows(InvalidProbabilityException.class, () -> {new MathProbability(0.5).validate();});
    }

    @Test
    public void shouldReturnFalseIfNullIsPassedForComparison() {
        assertFalse(new MathProbability(0.5).equals(null));
    }

    @Test
    public void shouldReturnFalseIfAStringIsPassedForComparison() {
        assertFalse(new MathProbability(0.5).equals("Probability"));
    }
}
